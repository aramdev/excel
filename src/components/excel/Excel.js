import { $ } from "@core/dom"
import { Emitter } from "@core/Emitter"

export class Excel {
  constructor(selector, options) {
    this.$el = $(selector)
    this.components = options.components || []
    this.store = options.store
    this.emitter = new Emitter()
    console.log(this.store)
  }

  getRoot () {
    const $root = $.create('div', 'excel')
    const componentOptions = {
      emitter: this.emitter,
      store: this.store
    }
    this.components = this.components.map(Components => {
      const $el = $.create('div', Components.className)
      const component = new Components($el, componentOptions)
      $el.html(component.toHTML())
      $root.append($el)
      return component
    })

    return $root
  }

  render () {
    this.$el.append(this.getRoot())
    this.components.forEach((component) => {
      component.init()
    })
  }

  destroy () {
    this.components.forEach((component) => {
      component.destroy()
    })
  }
}


