import {DomListener} from '@core/DomListener'

export class ExcelComponent extends DomListener {
  constructor($root, options = {}) {
    super($root, options.listeners)
    this.name = options.name || ''
    this.emitter = options.emitter
    this.store = options.store
    this.unsubscribers = []
    this.storeSub = null
    this.prepare()
  }

  prepare () {}

  toHTML () {
    return ''
  }

  $emit (event, ...args) {
    const unsub = this.emitter.emit(event, ...args)
    this.unsubscribers.push(unsub)
  }

  $on (event, fn) {
    this.emitter.subscribe(event, fn)
  }

  $dispatch (action) {
    this.store.dispatch(action)
  }

  $subscribe(fn) {
    this.storeSub = this.store.subscribe(fn)
  }

  init () {
    this.initDomListeners()
  }
  
  destroy () {
    this.removeDomListeners()
    this.unsubscribers.forEach(u => u())
    this.storeSub.unsubscribe()
  }
}