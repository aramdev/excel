import { capitalize } from "@core/utils"

export class DomListener {
  constructor($root, listeners = []) {
    if(!$root){
      throw new Error(`No $root provided for DomListener`)
    }
    this.$root = $root
    this.listeners = listeners
  }

  initDomListeners () {
    this.listeners.forEach( listener => {
      const method = getMethodName(listener)
      // console.log(this.$root)
      if (!this[method]){
        throw new Error(`
          Method ${method} in not implemented ${this.name || ''} Component
        `)
      }
      this[method] = this[method].bind(this)
      this.$root.on(listener, this[method])
    })
  }

  removeDomListeners () {
    console.log(10)
    this.listeners.forEach( listener => {
      const method = getMethodName(listener)
      // console.log(this.$root)
      if (!this[method]){
        throw new Error(`
          Method ${method} in not implemented ${this.name || ''} Component
        `)
      }
      this.$root.off(listener, this[method])
    })
  }
}


function getMethodName (eventName) {
  return 'on' + capitalize(eventName)
}