import {Excel} from '@/components/excel/Excel.js';
import './scss/index.scss';
import {Header} from '@/components/header/Header';
import {Toolbar} from '@/components/toolbar/Toolbar';
import {Formula} from '@/components/formula/Formula';
import {Table} from '@/components/table/Table';
import { createStore, CreateStore } from '@core/createStore';
import { rootReducer } from '@/store/rootReducer';
import { storage } from '@core/utils';

const store = createStore(rootReducer, storage('excel-state'))
// const store = new CreateStore(rootReducer)

store.subscribe(state => {
  console.log('App State', state)
  storage('excel-state', state)
})

const excel = new Excel('#app', {
  components: [Header, Toolbar, Formula, Table],
  store
});

excel.render();
